# C++ Programming
C++ is one of my, no it is my favorite programming language. I started coding at the age of 17 from my college "Nilgiri College of arts and science-Tamil Nadu". My BSc in computer science gave me a nice base. During my first few weeks 'the time when I get familiar with my course ' i become so much amazed about the not only possibilities and also about cool amazing life with a lot of new things to learn and lot and lots of fun. I can always recall it even when I'm just twelve years old I had been fascinated to learn new things that I couldn't comprehend, I know its strange. Back to the topic... My first programming language was C. I loved C very much, it was easy for me and also fun. C introduced me to this gorgeous new wonderland. In the second semester, I learned C++, and I had to admit it was truly awesome. For the first time in my life, I thought that I got a new super power. C++ made me a programmer teach me to solve problems, it showed me this wonderful world. The structure of C++ teaches me to understand any programming language easily, and its object-oriented programming concepts are deep and teach me how the objects and classes work. these a few of my early day's codes that I have done from college and home and I believe that it's enough to interest this language.
<br/>
### Languages & tools :
[<img align="left" alt="Visual Studio Code" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/visual-studio-code/visual-studio-code.png">][vscode]
[<img align="left" alt="Git" width="26px" src="https://github.com/AbhilashTUofficial/AbhilashTuofficial/blob/main/logos/tools/git.png">][github]
[<img align="left" alt="Terminal" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/terminal/terminal.png">][github]
[<img align="left" alt="C" width="32px" src="https://github.com/AbhilashTUofficial/AbhilashTuofficial/blob/main/logos/tools/c.png">][c/c++]
[<img align="left" alt="C++" width="26px" src="https://github.com/AbhilashTUofficial/AbhilashTuofficial/blob/main/logos/tools/cpp.png">][c/c++]
<br/><br/>

![GitHub last commit](https://img.shields.io/github/last-commit/AbhilashTUofficial/Cpp-programming?color=magenta&label=Last%20Commit%3A&style=for-the-badge)
![GitHub code size in bytes](https://img.shields.io/github/languages/code-size/AbhilashTUofficial/Cpp-programming?color=magenta&label=Repo%20Size%3A&style=for-the-badge)
### Connect with me :  
<a href="https://github.com/AbhilashTUofficial" target="_blank">
<img src=https://img.shields.io/badge/github-%2324292e.svg?&style=for-the-badge&logo=github&logoColor=white alt=github style="margin-bottom: 5px;" />
</a>
<a href="https://stackoverflow.com/story/abhilash-tu" target="_blank">
<img src=https://img.shields.io/badge/stackoverflow-%23F28032.svg?&style=for-the-badge&logo=stackoverflow&logoColor=white alt=stackoverflow style="margin-bottom: 5px;" />
</a>
<a href="https://www.linkedin.com/in/abhilash-tu-160630190/" target="_blank">
<img src=https://img.shields.io/badge/linkedin-%231E77B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white alt=linkedin style="margin-bottom: 5px;" />
</a>
<a href="https://medium.com/@abhilash-tu" target="_blank">
<img src=https://img.shields.io/badge/medium-%23292929.svg?&style=for-the-badge&logo=medium&logoColor=white alt=medium style="margin-bottom: 5px;" />
</a>
<a href="https://dev.to/abhilashtuofficial" target="_blank">
<img src=https://img.shields.io/badge/dev.to-%2308090A.svg?&style=for-the-badge&logo=dev.to&logoColor=white alt=devto style="margin-bottom: 5px;" />
</a>
<a href="https://codepen.io/abhilash-tu" target="_blank">
<img src=https://img.shields.io/badge/codepen-%23131417.svg?&style=for-the-badge&logo=codepen&logoColor=white alt=codepen style="margin-bottom: 5px;" />
</a>
<a href="https://twitter.com/Abhilash_TU" target="_blank">
<img src=https://img.shields.io/badge/twitter-%2300acee.svg?&style=for-the-badge&logo=twitter&logoColor=white alt=twitter style="margin-bottom: 5px;" />
</a>
<a href="https://dribbble.com/Abhilash_Tu" target="_blank">
<img src=https://img.shields.io/badge/dribbble-%23E45285.svg?&style=for-the-badge&logo=dribbble&logoColor=white alt=dribbble style="margin-bottom: 5px;" />
</a>
<a href="https://www.facebook.com/Abhilashtuofficial" target="_blank">
<img src=https://img.shields.io/badge/facebook-%232E87FB.svg?&style=for-the-badge&logo=facebook&logoColor=white alt=facebook style="margin-bottom: 5px;" />
</a>
<a href="https://www.instagram.com/abhilash_tu/" target="_blank">
<img src=https://img.shields.io/badge/instagram-%23000000.svg?&style=for-the-badge&logo=instagram&logoColor=white alt=instagram style="margin-bottom: 5px;" />
</a>
<a href="https://www.youtube.com/channel/UC8iP2LKB-V1g2jMTbe6Pb4Q" target="_blank">
<img src=https://img.shields.io/badge/youtube-%23EE4831.svg?&style=for-the-badge&logo=youtube&logoColor=white alt=youtube style="margin-bottom: 5px;" />
</a>
<a href="https://www.behance.net/abhilashstorm" target="_blank">
<img src=https://img.shields.io/badge/behance-%23191919.svg?&style=for-the-badge&logo=behance&logoColor=white alt=behance style="margin-bottom: 5px;" />
</a>   
<a href="https://discord.com/" target="_blank">
<img src=https://img.shields.io/badge/discord-%23191919.svg?&style=for-the-badge&logo=discord&logoColor=white alt=behance style="margin-bottom: 5px;" />
</a>   
<br/>

[website]: https://abhilashtuofficial.github.io/
[youtube]: https://www.youtube.com/channel/UC8iP2LKB-V1g2jMTbe6Pb4Q
[instagram]: https://www.instagram.com/abhilash_tu/
[linkdein]: https://www.linkedin.com/in/abhilash-tu-160630190/
[vscode]: https://code.visualstudio.com/
[github]: https://github.com/AbhilashTUofficial
[web]: https://github.com/AbhilashTUofficial/Web-development
[js]: https://github.com/AbhilashTUofficial/JavaScript-programming
[python]: https://github.com/AbhilashTUofficial/Python-programming
[dart]: https://github.com/AbhilashTUofficial/CloneApps
[c/c++]: https://github.com/AbhilashTUofficial/Cpp-programming
[flutter]: https://github.com/AbhilashTUofficial/CloneApps
[java]: https://github.com/AbhilashTUofficial/java-programming
[android]: https://github.com/AbhilashTUofficial/CloneApps
[behance]: https://www.behance.net/abhilashstorm
